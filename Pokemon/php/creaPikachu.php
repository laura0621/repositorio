<?php
require_once 'conexion.php';

$cadena = "http://pokeapi.co/api/v2/pokemon/";
$idPoke = 25;
$array_entradas_pokedex;
$tipos_stats_necesarios = ["speed","special-defense","special-attack","defense","attack","hp"];
$array_stats;
$stats;
$array_tipos;
$texto_tipos = "";
$idioma = "es";

$data = file_get_contents($cadena.$idPoke.'/');
if($data != ""){
  $infopoke = json_decode($data);
  $nombre_pokemon = $infopoke->name;
  $imagen_pokemon = $infopoke->sprites->front_default;
  $array_tipos = $infopoke->types;
  $array_stats = $infopoke->stats;

  # For para obtener las caracteristicas de un pokemon, el orden de las caracteristicas la dicta el arreglo
  # tipos_stats_necesarios
  for ($i=0; $i < count($tipos_stats_necesarios); $i++) {
    foreach($array_stats as $obj){
      if ($tipos_stats_necesarios[$i] == $obj->stat->name) {
        $stats[] = $obj->base_stat;
      }
    }
  }

  # Se busca los tipos de los pokemons y se separa por coma en una sola variable
  foreach($array_tipos as $obj){
    $texto_tipos .= $obj->type->name . ",";
  }


  $mysqli->query("insert into pokemon values('$idPoke','$nombre_pokemon','$imagen_pokemon','$stats[5]','$stats[4]','$stats[3]','$stats[2]','$stats[1]','$stats[0]')");
}

$cadena_ataques = 'http://pokeapi.co/api/v2/move/';
# se llena primero los ataques de pikachu de forma aletoria
for ($i=0; $i < 4; $i++) {
  $id_move =  mt_rand(1,719);
  $data_moves = file_get_contents($cadena_ataques.$id_move.'/');
  if($data_moves != ""){
    $movimientos = json_decode($data_moves);
    $nombre_ataque = "";
    $puntosp = $movimientos->pp;
    $pres = $movimientos->accuracy;
    $poder = $movimientos->power;
    $tipo_movimiento = $movimientos->type->name;
    $entradas_en_diferentes_idiomas = $movimientos->names;
    foreach ($entradas_en_diferentes_idiomas as $obj) {
      if($idioma == $obj->language->name){
        $nombre_ataque = $obj->name;
      }
    }
    $mysqli->query("insert into ataque values('$id_move','$nombre_ataque','$tipo_movimiento','$poder','$pres','$puntosp')");
    $mysqli->query("insert into movimientos values('25','$id_move')");
  }
}


$mysqli->close();
?>
